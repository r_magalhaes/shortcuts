## Vim shortcuts 

# Basics

## Enter Insert Mode

- i

## Visual Mode (Select and move text)

- v

## Return to Normal Mode

- ESC

## Quit Vim

- :q

## Write file (Doesn't exit Vim)

- :w

## Write and quit

- :wq

## Quit but lose changes

- :q!

## Help

- :h

<br>

# File Operations

## Append to end of current character

- a

## Delete current character

- x

## Undo last comand

- u

## Redo previous command

- ctrl+R

## Delete

- d

## Open line below the cursor and enter Insert Mode

- o

## Open line above the cursor and enter Insert Mode

- O

<br>

# Visual Mode Operations

## Delete word/sentence

- d

## Yank/Copy

- y

## Undo

- u

## Paste

- p

## Change (deleted the text selected in visual mode, and moves you straight to insert mode)

- c

## Move one word forwards

- w

## Move one word backwards

- b

## Move to the end of the word

- e

<br>

# Visual Line Mode (Copy and paste text)

## Enter Visual Line Mode

- Shift+v

## Delete

- d

## Increase indent

- Shift+>

## Decrease indent

- Shift+<

## Yank/Copy

- y

<br>

# File opening/creation

## Open/Create if not exist

- vi file.txt

## Open two files together

- vi -O fist.txt second.txt


### Toggling between files

- Control+w then w

### Selecting

- Control+w then cursor
